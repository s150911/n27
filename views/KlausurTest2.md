# Klausur / Test 2 am 14.01.2020 um 09:10

1. Fehler finden und auf Papier dokumentieren
2. alles, was in T1 / K1 relevant war
3. Selber etwas ausprogrammieren (GUI oder server.js)
4. SQL. Evtl. einen unbekannten SQL-Befehl anhand einer gegebenen Dokumentation selbst erstellen
5. if und else (auch verschachtelt). Bitte auch die alten if-else-SAchen anschauen im Trainingsordner
6. Symmetrische und asymmetrische Verschlüsselung erklären, gegeneinander abgrenzen. Den Sinn jeweils erklären.     Die Implementtation am Rechner kurz beschreiben.


## Beispiel zu 5 zu If-Else:

```Javascript
// Wenn Schüler / eine Schülerin nicht volljährig ist, wird "Eintritt verweigert"

var darfHinein = true
var istVolljaehrig = true;

if(istVolljaehrig){
    darfHinein = true
    Console.Log("Der Schüle / die Schülerin darf hinein:" + darfHinein)
}else{
    darfHinein = false
    Console.Log("Der Schüle / die Schülerin darf hinein:" + darfHinein)
}

```

```Javascript
// Wenn Schüler / eine Schülerin nicht volljährig ist, wird "Eintritt verweigert"

var darfHinein = ""
var alter = 18;

if(alter >= 18){
    darfHinein = "ja"
}else{
    darfHinein = "nein"
}



Console.Log("Der Schüle / die Schülerin darf hinein:" + darfHinein)

```
```Javascript
// Wenn Schüler / eine Schülerin nicht volljährig ist, wird "Eintritt verweigert"
// Schülerinnen zahlen 3 Euro
// Schüler zahlen 4 Euro

var darfHinein = true
var istVolljaehrig = true;
var geschlecht = "w"


if(istVolljaehrig){
    darfHinein = true

    if(geschlecht === "w"){
    Console.Log("Der Schüle / die Schülerin darf hinein:")
    }


}else{
    darfHinein = false
    Console.Log("Der Schüle / die Schülerin darf hinein:" + darfHinein)
}

```